﻿var GameObjects = window.GameObjects || {};
var crashY;
GameObjects.Character = (function () {
    return {

        sprite: "",
        bFacingRight: "",
        Name: "",
        
        init: function (CharName) {
            bFacingRight = true;
            Name = (CharName);
            sprite = game.add.sprite(320, 23800, 'mainCar');
           
            game.camera.follow(sprite);
            game.physics.enable(sprite, Phaser.Physics.ARCADE);
            //sprite.body.bounce.y = 0.2;
            //sprite.body.collideWorldBounds = false;
            //sprite.body.setSize(0, 0, 57, 75);

            sprite.animations.add('onDrive', [0]);
            sprite.animations.add('stand', [0]);
            sprite.animations.add('collide', [1,2, 3, 4,0],false);
            sprite.animations.play('stand', 8, true);
            sprite.anchor.setTo(.5, .5);
            game.camera.follow(sprite);
        },
        update: function(){
            
        },
        GetName: function () {
            return Name;
        },
        GetSprite: function () {
         
            return sprite;
        },
        explosion: function () {


        },
        GetPosition: function(){
            return sprite.position;
        },
        zeroVelocityLR: function(){
            sprite.body.velocity.x = 0;
        },
        zeroVelocityUp: function () {
            if (sprite.body.velocity.y<0){
                sprite.body.velocity.y++;
            }
        },

        PressRight: function () {
            //sprite.x++;
           
                sprite.body.velocity.x = 200;
                sprite.animations.play('walk', 8, true);
                console.log("right");
                       
        },
        PressLeft: function () {
           
               sprite.body.velocity.x = -200;
               sprite.animations.play('walk', 8, true);
               console.log("left");
            
        },
        PressDown: function () {
            if (sprite.body.velocity.y < 0)
            {
                if (sprite.body.velocity.y > -5) {

                    sprite.body.velocity.y += Math.abs(sprite.body.velocity.y);
                
                }

                else{

                    sprite.body.velocity.y+=5;
                }
                  }
                

            sprite.animations.play('walk', 8, true);

            console.log("down");


            },
            //sprite.body.velocity.y++;

            //sprite.animations.play('walk', 8, true);

            //console.log("down");
        
        PressUp: function () {
            if (sprite.body.velocity.y > -600)
                sprite.body.velocity.y -= 7;
             
            sprite.animations.play('walk', 8, true);

            console.log("up");
        },

        collideLeft: function () {
          
                //sprite.body.velocity.x = -280 ;
            
                sprite.animations.play('collide', 4);
                console.log("collide");
                crashY = sprite.position.y;
                
            
        },

        collideRight: function () {
          
            //sprite.body.velocity.x = -280;
      
            
                sprite.animations.play('collide', 4);
                console.log("collide");
                crashY = sprite.position.y;
                           
            
        },
        Stand: function () {
            sprite.body.velocity.y = 0;
            sprite.animations.play('stand', 8, true);
            console.log("stand");
        },

        startBegin: function(){
            game.add.tween(sprite).to({ x: game.world.centerX, y: crashY }, 1000, Phaser.Easing.Linear.None, true);
            sprite.body.velocity.y = 0;
    }

    };
}
    );