﻿var showLogs = true;

if (showLogs) {
 
    console.log('inside screenManager');
   
  
}
var game = new Phaser.Game(gameWidth, gameHeight, Phaser.AUTO, 'RoadFighter', { preload: preload, create: create, update: update });
var gameWidth = 600;
var gameHeight = 24000;
var keys;
var redCar;
var blueCar;
var blueCarStart0;
var blueCarStart1;
var blueCarStart2;
var blueCarStart3;
var blueCarStart4;
var blueCarStart5;
var startCarGroup;
var goBlue = -1;
var carCount = 100;
var carArray = [];
var randXarr = [140, 220, 300, 380, 460, 510];
function preload() {
    if (showLogs) {
        console.log("ScreenManager - preload");
    }


    game.load.tilemap('background', 'Levels/track.json', null, Phaser.Tilemap.TILED_JSON);
    game.load.image('tiles', 'Levels/tiled.png');
    game.load.image('tiles2', 'Levels/tiled2.png');
    game.load.image('tiles3', 'Levels/tiled3.png');
    game.load.atlas('mainCar', 'Sprites/carSprite.png', 'Sprites/carSprite.json', Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);
    game.load.image('blueCar', 'Pictures/blue_car.png');

    //game.load.image('redCar', 'Pictures/araba.png');
    //game.load.tilemap('bg', 'Levels/track.json', null, Phaser.Tilemap.TILED_JSON);
    //game.load.image('tiles', 'Levels/track.png');
   
    //game.load.atlas('bg', 'Sprites/track.png', 'Sprites/track.json', Phaser.Loader.TEXTURE_ATLAS_JSON_ARRAY);

}

var map;
var layer;
var collideLayer;

//var isFinished = false;



function create() {
    if (showLogs)
        console.log("ScreenManager - create");

    map = game.add.tilemap('background');
    map.addTilesetImage('tiled', 'tiles');
    map.addTilesetImage('tiled2', 'tiles2');
    map.addTilesetImage('tiled3', 'tiles3');
    layer = map.createLayer('road');
    CollideLayer = map.createLayer('collider');
    map.setCollisionBetween(1, 100, true, CollideLayer);
    layer.resizeWorld();

    blueCars = game.add.group();

    blueCarStart0 = game.add.sprite(200, 23500, 'blueCar');
    blueCarStart1 = game.add.sprite(400, 23500, 'blueCar');
    blueCarStart2 = game.add.sprite(200, 23600, 'blueCar');
    blueCarStart3 = game.add.sprite(400, 23600, 'blueCar');
    blueCarStart4 = game.add.sprite(200, 23700, 'blueCar');
    blueCarStart5 = game.add.sprite(400, 23700, 'blueCar');
    blueCarStart0.scale.setTo(0.9);
    blueCarStart1.scale.setTo(0.9);
    blueCarStart2.scale.setTo(0.9);
    blueCarStart3.scale.setTo(0.9);
    blueCarStart4.scale.setTo(0.9);
    blueCarStart5.scale.setTo(0.9);

    startCarGroup = game.add.group()
    startCarGroup.add(blueCarStart0);
    startCarGroup.add(blueCarStart1);
    startCarGroup.add(blueCarStart2);
    startCarGroup.add(blueCarStart3);
    startCarGroup.add(blueCarStart4);
    startCarGroup.add(blueCarStart5);

    game.add.tween(startCarGroup).to({ y: -23500 }, 19500, Phaser.Easing.Linear.None, true);

    for (i = 0; i < carCount; i++)
    {
        var randomSelectX = game.rnd.integerInRange(0,5);
        
      
        var randY = game.rnd.integerInRange(0, 23100);
      
        blueCar = game.add.sprite(randXarr[randomSelectX], randY, 'blueCar');
        carArray.push(blueCar);
    }
        //game.physics.arcade.enable(redCar, blueCars, Phaser.Physics.ARCADE);


    //game.physics.enable(spriteCar, Phaser.Physics.ARCADE);

    //map = game.add.tilemap('bg');
    //map.addTilesetImage('tiled', 'tiles');

    //layer = map.createLayer('road');
    //CollideLayer = map.createLayer('collider');
    //map.setCollisionBetween(1, 100, true, CollideLayer);


    //spriteCar = game.add.sprite(330, 3900, 'mainCar');
    //spriteCar.name = 'mainCar';
    
    
    //layer.resizeWorld();

    keys = game.input.keyboard.createCursorKeys();

    redCar = new GameObjects.Character();
    redCar.init("Car");
    //console.log("name 5 " + redCar.GetName());

}


function update() {
    redCar.update();
    console.log(startCarGroup.position.y);
    //if (redCar.GetPosition().y < 400) {
    //    //if (!isFinished) {
    //    //    console.log("hello");
    //    //    isFinished = true;
    //    //}
    //}

    if (startCarGroup.position.y <= -15300)
    {
        blueCarStart0.kill();
        blueCarStart1.kill();
        blueCarStart2.kill();
        blueCarStart3.kill();
        blueCarStart4.kill();
        blueCarStart5.kill();
        console.log("killllllllllll");
    }

   checkCollision();

    game.physics.arcade.collide(redCar.GetSprite(), CollideLayer);
    
    if (keys.up.isDown) {
        redCar.PressUp();
    } else if (keys.down.isDown) {
        redCar.PressDown();
    }

    if (keys.right.isDown) {
        redCar.PressRight();
    }
    else if (keys.left.isDown) {
        redCar.PressLeft();
    }
    if (!keys.right.isDown && !keys.left.isDown) {
        redCar.zeroVelocityLR();
    }
    if (!keys.up.isDown) {
        redCar.zeroVelocityUp();
    }
    if(redCar.GetPosition().x<138)
    {
        redCar.collideLeft();
        redCar.startBegin();
    }

    if (redCar.GetPosition().x > 523) {
        redCar.collideRight();
        redCar.startBegin();
    }
 


}

function checkCollision() {
    var boundsA = redCar.GetSprite().getBounds();
    
    carArray.forEach(function (item) {
        var boundsB = item.getBounds();
        var bCar = item;
        item.position.y -= 3;
        if(Phaser.Rectangle.intersects(boundsA, boundsB)){
            redCar.startBegin();
            redCar.collideRight();
            return;
        }
    })
}

function render() {

    game.debug.cameraInfo(game.camera, 32, 32);
    game.debug.spriteCoords(redCar, 32, 500)

}
